Reflexiona lo aprendido y contesta las siguientes preguntas con tus palabras:
1. ¿Cómo defines el concepto de clase?
         Es una plantilla que contiene caracterísiticas de una funcionalidad
         Dentro de la clase se van a definir los atributos y métodos que deberían interactuar entre ellos y corresponder
         intrínsicamente a la función principal de esta plantilla.
        
2. ¿Cómo defines el concepto de objeto?
         Es la construcción en memoria de una Clase. 
         Al crearla se le pueden ya definir sus atributos y así lograr usar los métodos


3. ¿Cómo defines el concepto de atributo?
         
         las caracterísiticas principales y canónicas de una clase. 

4. ¿Cómo defines el concepto de método?
         
         son las funciones (fórmulas) o el conjunto de relaciones entre los atributos de la clase

5. ¿Qué es lo que pasa cuando instanciamos una clase?

        en memoria, creamos el un objeto y con ella se tiene acceso a definir todos sus atributo, acceso a todos sus métodos
        y con esa instanciación ya podemos usarel objeto de la clase