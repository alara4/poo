﻿using System;

namespace _1erEjercicio
{
    class Program
    {
        static void Main(string[] args)
        {

            Carro elCarro = new Carro();
            elCarro.Marca = "Honda";
            elCarro.Modelo = "Civic";
            elCarro.Anio = 2012;
            elCarro.Color = "plata";
            elCarro.Velocidades = "cinco";

            Console.WriteLine( elCarro.infPrincipal());
            Console.WriteLine( elCarro.masDetalle());
            elCarro.pintaInfo();

        }
    }

    class Carro
    {
        //Atributos de la clase carro
        public string Marca { get; set; }
        public string Modelo { get; set;}
        public int Anio { get; set;}
        public string Color { get; set;}
        public string Velocidades { get; set;}

        //Métodos de la clase carro
        public string infPrincipal()
        {
            return "El carro es un "+ Marca+ ", modelo "+ Modelo;

        }

        public string masDetalle()
        {
            return "El carro es de color " + Color + " y además de "+ Velocidades +" velocidades!!!!";
        }

        public void pintaInfo()
        {
            Console.WriteLine( "El carro es un "+ Marca+ ", modelo "+ Modelo +  " del año {0}", Anio);
        }
    }
}
